$(document).ready(function(){
    $("#searchButton").on("click", function(){
        search();
    });
    $("tbody").on("click", ".like", function(){
        var id = $(this).attr('id').substring(1);
        var likes = $(this).prev().text() * 1+ 1;
        like(id, likes, $(this).prev())
    });
    $("#topButton").on("click", function(){
        mostLiked();
    })
});


function search(){
    var searchParam = $("#searchBox").val();
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes",
        data: {q: searchParam},
        dataType: "json",
        type: "GET",
        success: function(response){    
            var i;
            $("tbody").html("");
            for(i = 0;i < response.items.length;i++){
                var names = "";
                var results = {};
                if("authors" in response.items[i].volumeInfo){
                    for(j = 0;j < response.items[i].volumeInfo.authors.length;j++){
                        names += response.items[i].volumeInfo.authors[j] + ", ";
                    }
                    names = names.substring(0, names.length-2);
                }else{
                    names = "-";
                }
                input(response.items[i].id, response.items[i].volumeInfo.title, names, 0);
            }
        }
    });
};

function input(id, title, authors, likes){
    $.ajax({
        url: "books",
        data: {'id': id, 'title': title, 'authors': authors, 'likes': likes},
        dataType: "json",
        type: "GET",
        success: function(response){
            results = "<tr><td>" + response.title + "</td><td>" + response.authors + "</td><td><div class='text-center'><p>" + response.likes + "</p><button class='like' id=#" + response.id + ">Like</button></div></td></tr>";
            $("tbody").html($("tbody").html() + results);
            sort();
        }
    });
};

function like(id, likes, $tagName){
    $.ajax({
        url:"books",
        data: {'id': id, 'likes': likes},
        dataType: 'json',
        type: 'GET',
        success: function(response){
            $tagName.text(response.likes);
        }
    })
}

function sort(){
    var switching = true;
    var table = document.getElementById("searchResult");
    while(switching){
        switching=false;
        var rows = table.rows;
        var i;
        var swap;

        for(i = 1;i < rows.length-1;i++){
            swap = false;
            first = rows[i].getElementsByTagName("p")[0];
            second = rows[i+1].getElementsByTagName("p")[0];
            if(Number(first.innerHTML) < Number(second.innerHTML)){
                swap = true;
                break;
            }
        }

        if(swap){
            rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
            switching = true;
        }
    }
}

function mostLiked(){
    $.ajax({
        url: "likes",
        dataType: "json",
        type: "GET",
        success: function(response){
            var i;
            $(".modal-body").find("tbody").html("");
            for(i = 0;i < 5;i++){
                results = "<tr><td>" + response.order[i].title + "</td><td>" + response.order[i].authors + "</td><td><div class='text-center'><p>" + response.order[i].likes + "</p></div></td></tr>";
                $(".modal-body").find("tbody").html($(".modal-body").find("tbody").html() + results);
            }
        }
    });
}