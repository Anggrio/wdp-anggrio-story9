from django.db import models

# Create your models here.
class Books(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    title = models.CharField(max_length=255)
    authors = models.CharField(max_length=255)
    likes = models.IntegerField()

    def __str__(self):
        return self.id