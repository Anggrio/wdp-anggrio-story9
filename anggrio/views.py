from django.shortcuts import render
from django.http import JsonResponse
from .models import Books
# Create your views here.
def bookPage(request):
    return render(request, 'book-page.html')

def booksInfo(request):
    if request.method == 'GET':
        id = request.GET['id']
        likes = request.GET['likes']
        if Books.objects.filter(id=id).exists():
            response = Books.objects.get(id=id)
            if response.likes < int(likes):
                response.likes = likes
                response.save()
            return JsonResponse({'id': response.id, 'title': response.title, 'authors': response.authors, 'likes': response.likes})
        else:
            title = request.GET['title']
            authors = request.GET['authors']
            Books.objects.create(id=id, title=title, authors=authors, likes=likes)
            response = Books.objects.get(id=id)
            return JsonResponse({'id': response.id, 'title': response.title, 'authors': response.authors, 'likes': response.likes})

def likesInfo(request):
    if request.method == 'GET':
        ordered_likes = Books.objects.order_by('-likes')[:5]
        response = list(ordered_likes.values())
        return JsonResponse({'order': response})