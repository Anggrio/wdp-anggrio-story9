from django.urls import path

from . import views

urlpatterns = [
    path('', views.bookPage, name='bookPage'),
    path('books', views.booksInfo, name='booksInfo'),
    path('likes', views.likesInfo, name='likesInfo'),
]