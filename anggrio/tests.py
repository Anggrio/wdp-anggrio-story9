from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Books
from .views import bookPage, booksInfo, likesInfo
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Story9UnitTests(TestCase):

    def test_book_search_and_display_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_book_search_and_display_page_using_profile_page_func(self):
        found = resolve('/')
        self.assertEqual(found.func, bookPage)

    def test_book_search_and_display_page_using_profile_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'book-page.html')

    def test_book_search_and_display_page_is_completed(self):
        request = HttpRequest()
        response = bookPage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Search', html_response)

    def test_books_info_page_url_is_exist(self):
        response = Client().get('/books', {'id': '1', 'title': 'test', 'authors': 'test', 'likes': 0})
        self.assertEqual(response.status_code, 200)

    def test_books_info_page_using_books_info_func(self):
        found = resolve('/books')
        self.assertEqual(found.func, booksInfo)

    def test_books_info_page_returns_json_book_info(self):
        response = Client().get('/books', {'id': '1', 'title': 'test', 'authors': 'test', 'likes': 0})
        json_response = response.content.decode('utf8')
        self.assertJSONEqual(json_response, {'id': '1', 'title': 'test', 'authors': 'test', 'likes': 0})

    def test_likes_info_page_url_is_exist(self):
        response = Client().get('/likes')
        self.assertEqual(response.status_code, 200)

    def test_likes_info_page_using_likes_info_func(self):
        found = resolve('/likes')
        self.assertEqual(found.func, likesInfo)

class Story9FunctionalTests(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_can_search_for_books(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Anggrio's Book Search", self.browser.title)
        time.sleep(5)

        searchBox = self.browser.find_element_by_id("searchBox")
        searchButton = self.browser.find_element_by_id("searchButton")
        searchBox.send_keys("Test")
        searchButton.click()
        time.sleep(5)

        table = self.browser.find_element_by_id("searchResult")
        self.assertIn("Test", table.text)

    def test_can_increment_like_of_a_book(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Anggrio's Book Search", self.browser.title)
        time.sleep(5)

        searchBox = self.browser.find_element_by_id("searchBox")
        searchButton = self.browser.find_element_by_id("searchButton")
        searchBox.send_keys("Test")
        searchButton.click()
        Books.objects.all().delete()
        time.sleep(5)

        firstLikeButton = self.browser.find_element_by_class_name("like")
        firstLikeDisplay = self.browser.find_element_by_tag_name("p")
        self.assertEqual("0", firstLikeDisplay.text)
        firstLikeButton.click()
        time.sleep(5)

        firstLikeDisplay = self.browser.find_element_by_tag_name("p")
        self.assertEqual("1", firstLikeDisplay.text)

    def test_can_show_modal_of_most_liked_books(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Anggrio's Book Search", self.browser.title)
        time.sleep(5)

        searchBox = self.browser.find_element_by_id("searchBox")
        searchButton = self.browser.find_element_by_id("searchButton")
        searchBox.send_keys("Test")
        searchButton.click()
        Books.objects.all().delete()
        time.sleep(5)

        firstLikeButton = self.browser.find_element_by_class_name("like")
        firstLikeDisplay = self.browser.find_element_by_tag_name("p")
        self.assertEqual("1", firstLikeDisplay.text)
        firstLikeButton.click()
        time.sleep(5)

        modalButton = self.browser.find_element_by_id("topButton")
        modalButton.click()
        time.sleep(5)

        self.assertIn("2", self.browser.find_element_by_id("likeModal").text)